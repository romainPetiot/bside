# Bside

Prérequis :

- Node.js (https://nodejs.org/)
- npm (https://www.npmjs.com/)
- Wordpress (http://wordpress.org/latest.tar.gz)

1 - Installer Wordpress

2 - Installer le(s) pluggin(s) suivant(s) :
```console
ACF PRO
```

3 - Ajouter le thème Bside à l'emplacement suivant :
```console
[projet] / wp-content / themes / bside
```

4 - Installer les dépendances avec npm
```console
$ npm install
```

5 - Lancer le script
```console
$ npm run watch
```

6 - Lancer BrowserSync [optionnel]
```console
$ npm run start
```

-----------

# Notes complémentaires :

Retirer les thèmes Wordpress inutiles (twentytwenty, twentynineteen, ect …)
```console
$ cd wp-content/themes/
$ rm -rf twenty
```

Modifier le chemin des scripts en fonction de l'environnement local
Chemin par défaut : 
```console
'https://agencebside.local/'
```
dans package.json (ligne 44)

Pluggins Wordpress utilisés :
- ACF PRO - b3JkZXJfaWQ9OTE1Mzh8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE2LTEwLTEyIDE0OjMyOjI3
- Yoast SEO
- WP Rocket