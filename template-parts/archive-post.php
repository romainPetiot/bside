<?php
//setup_postdata( $post );
$term_obj_list = get_the_terms( $post, 'category' );
?>

<!-- Container de l'article -->
<article class="post-card">

	<!-- Image mise en avant -->
	<a class="link-image" href="<?php the_permalink(); ?>"  title="<?php // the_title(); ?>">

		<?php 	
		if ( has_post_thumbnail() ) {
			// Post-thumbnail
			$image = get_post_thumbnail_id();
		} else {
			// Fallback image
			$image = get_field('image-fallback', 'options');
		} 

		$size = 'bside-post-excerpt';
		if( $image ) {
			echo wp_get_attachment_image( $image, $size );
		}
		?>
	</a>

	<!-- Content-->
	<div>

		<!-- Catégories -->
		<?php
		foreach ( $term_obj_list as $term_obj ) {
			echo '<a class="link-red" href="' . get_category_link($term_obj) .'">' . $term_obj->name . '</a> ';
		}
		?>

		<!-- Link -->
		<a class="post-content" href="<?php the_permalink(); ?>" title="<?php // the_title(); ?>">
			<!-- Titre -->
			<h2 class="h3-like"><?php the_title(); ?></h2>
			<!-- Extrait -->
			<div><?php the_excerpt(); ?></div>
		</a>

		<!-- Date -->
		<time class="small-text" datetime="<?php echo get_the_date( 'c' ); ?>2012-02-11"><?php echo get_the_date(); ?></time>
		
	</div>

</article>