<?php
setup_postdata( $post );
?>

<!-- Container de la réalisation -->
<article class="realisation-card">
	
	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		<!-- Image mise en avant -->
		<div class="realisation-thumb">

		<?php 	
		// 1 - Find image
		if ( has_post_thumbnail() ) {
			$id = get_post_thumbnail_id(); // Post-thumbnail
		} else {
			$id = get_field('image-fallback', 'options'); // Fallback image
		} 

		// 2 - Display image
		$size = 'bside-realisation-excerpt';
		if( $image ) {
			echo wp_get_attachment_image( $image, $size );
		}
		?>
		
		</div>
		<!-- Contenu -->
		<div class="realisation-content">
			<span>
				<!-- Titre -->
				<h3 class="lead-paragraph"><?php the_title(); ?></h3>
				<!-- Type -->
				<?php 
				$type = the_field( 'realisation_type', $post->ID );
				if ( $type ) {
					echo '<p>'. $type .'</p>';
				} 
				?>
			</span>
		</div>
	</a>

</article>