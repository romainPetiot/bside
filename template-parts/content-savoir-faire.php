<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Susty
 */

?>

<article id="post-<?php the_ID(); ?>" class="single-savoirfaire">

	<!-- Intro de l'article -->
	<header class="wrapper center" style="background-color: <?php the_field('color'); ?>">
		<!-- Titre -->
		<?php 
		$title = get_field('title');
			if ( $title ) {
				echo '<h1>' ;
				echo $title ;
				echo '</h1>';
			} else {
				the_title('<h1>', '</h1>');
			} 
		?>
		<img aria-hidden="true" src="<?php echo get_stylesheet_directory_uri(); ?>/image/flach-bas.png"  height="50" width="50">
	</header>

	<!-- Contenu-->
	<div id="raw-content">
		<?php the_content(); ?>
	</div>

</article><!-- #post-<?php the_ID(); ?> -->
