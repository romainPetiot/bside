<?php
setup_postdata( $post );
?>

<!-- Container de la réalisation -->
<article class="realisation-card">
	
	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">

		<!-- Image mise en avant -->
		<div class="realisation-thumb">

			<?php 
			$thumb = get_field('thumb-square',  $post->ID);
			$size = 'bside-realisation-excerpt';
			
			// A - Test for custom square thumbnail
			if ( $thumb ) {
				$image = $thumb;
			}

			else {

				// B - Test for post thumbnail 
				if ( has_post_thumbnail() ) {
					$image = get_post_thumbnail_id();
				} 
				
				// C - Fallback image
				else {
					$image = get_field('image-fallback', 'options');
				} 
			}
	
			// D - Display Image 
			if( $image ) {
				echo wp_get_attachment_image( $image, $size );
			}
			?>

		</div>

		<!-- Contenu -->
		<div class="realisation-content">
			<span>
				<!-- Titre -->
				<h3 class="lead-paragraph"><?php the_title(); ?></h3>
				<!-- Type -->
				<?php 
				$type = the_field( 'realisation_type', $post->ID );
				if ( $type ) {
					echo '<p>'. $type .'</p>';
				} 
				?>
			</span>
		</div>
	</a>

</article>