<?php
/**
 * Block Name: Bloc last-realisation
 */
?>

<?php 
	$changeColor = get_field( 'has-color');
	$color = get_field( 'color' );
	$textColor = "black";

	if ( $changeColor ) {
		if ( $color ) {
			$textColor = "white";
			$bgColor =  true;
		}
	}
?>

<section class="wrapper center v-padding-small <?php echo $textColor; ?>"
	<?php 
	if ($bgColor) {
		echo 'style="background-color :'. $color .';"' ;
	}
	?>>

	<?php 
	$title = get_field( 'title' );
	if ( !$title ) {
		$title = "Best-of";
	}
	?>

	<h2 class="h1-like margin-b"><?php echo $title ?></h2>

	<div id="listing-realisation">

	<?php

	global $post;

	if ( get_field( 'last_realisations' ) ) :

		$lastrealisations = get_field( 'last_realisations' );

	else :

		$lastrealisations = get_posts(
			array(
				'posts_per_page' => 6,
				'post_status'    => 'publish',
				'post_type'      => 'realisation',
			)
		);

	endif;

	if ( $lastrealisations ) :
		foreach ( $lastrealisations as $post ) :

			// Listing template : archive-realisation.php
			get_template_part( 'template-parts/archive-realisation' );

		endforeach;
		wp_reset_postdata();
	endif;
	?>

	</div>

	<!-- Archive Link -->
	<?php 
	$link = get_field('realisation-archive', 'option');
	if( $link ): 
		$link_url = $link['url'];
		$link_title = $link['title'];
		$link_target = $link['target'] ? $link['target'] : '_self';
    ?>
    <a class="link-archive link-default <?php if ($bgColor) { echo "stay-white"; }?> " href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo _e( 'Toutes les réalisations', 'bside' ); ?></a>
	<?php endif; ?>

</section>
