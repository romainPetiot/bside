<section class="blk-backstage white">

<?php

if ( have_rows( 'backstage_links' ) ) :

	while ( have_rows( 'backstage_links' ) ) :
		the_row();

    ?>
    
    <!-- Lien vers la page + Couleur du BG -->
    <a class="card-backstage" style="background-color:<?php the_sub_field( 'color' ); ?>" href="<?php the_sub_field( 'page_link' ); ?>">

      <?php 	
      // 1 - Find image
      
      $image = get_sub_field( 'background_image' );

      if ( $image  ) {
        $id = $image;
      }
      else {
        $id = get_field('image-fallback', 'options'); // Fallback
      } 

      // 2 - Display image
      $size = 'bside-realisation-excerpt';
      $src = wp_get_attachment_image_src( $id, $size );
      $srcset = wp_get_attachment_image_srcset( $id, $size );
      $sizes = wp_get_attachment_image_sizes( $id, $size );
      $alt = get_post_meta( $id, '_wp_attachment_image_alt', true); 
      ?>

      <!-- Image de BG -->
      <img src="<?php echo esc_attr( $src[0] );?>"
        srcset="<?php echo esc_attr( $srcset ); ?>"
        sizes="<?php echo esc_attr( $sizes );?>"
        alt="#" aria-hidden= "true" />

      
      <div>
        <!-- Titre -->
        <h2><?php the_sub_field( 'title' ); ?></h2>
        <!-- Texte -->
        <p><?php the_sub_field( 'text' ); ?></p>
        <!-- En savoir plus -->
        <?php 
        if ( get_sub_field( 'page_link' ) ) { ?>
          <p class="link-archive" ><?php echo _e( 'En savoir plus', 'bside' ); ?></p>
        <?php
        }
        ?>
      </div>
      
    </a>

		<?php

  endwhile;

endif;

?>

</section>