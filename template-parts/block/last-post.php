<?php
/**
 * Block Name: Bloc last-post
 */
?>

<section id="blk-last-post" class="wrapper center v-padding-regular white">
	
	<h2 class="h1-like margin-b"><?php _e( 'Mixtape', 'bside' ); ?></h2>

	<div id="listing-post">

	<?php

	global $post;

	if ( get_field( 'last-posts' ) ) :

		$lastposts = get_field( 'last-posts' );

	else :

		$lastposts = get_posts(
			array(
				'posts_per_page' => 3,
				'post_status'    => 'publish',
			)
		);

	endif;

	if ( $lastposts ) :
		foreach ( $lastposts as $post ) :

			setup_postdata( $post );
			
			// Listing template : archive-post.php
			get_template_part( 'template-parts/archive-post' );

		endforeach;
		wp_reset_postdata();
	endif;
	?>

	</div>

	<!-- Archive Link -->
	<?php 
	$link = get_field('post-archive', 'option');
	if( $link ): 
		$link_url = $link['url'];
		$link_title = $link['title'];
		$link_target = $link['target'] ? $link['target'] : '_self';
    ?>
    <a class="link-archive white" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo _e( 'Tous les articles', 'bside' ); ?></a>
	<?php endif; ?>

</section>
