<?php
/**
 * Block Name: Bloc title-paragraph-big-size
 */
?>

<!-- Block Title + Paragraph -->
<section class="narrow-wrapper v-padding-regular">

	<?php
	$content = get_field('content');
	if ( !$content ) :
	?>

		<div class="admin-infos">
			<b>Bloc Titre + Paragraph</b><br>
			<p>Renseigner les informations</p>
		</div>

	<?php else : ?>

		<!-- Titre -->
		<?php if ( get_field( 'title' ) ) : ?>
			<<?php echo (get_field( 'titre-level' ))?get_field( 'titre-level' ):'h2';?> class="h1-like"><?php the_field( 'title' ); ?></<?php echo (get_field( 'titre-level' ))?get_field( 'titre-level' ):'h2';?>>
		<?php endif; ?>

		<!-- Contenu -->
		<?php if ( get_field( 'content' ) ) : ?>
			<div class="lead-paragraph entry-content"><?php the_field( 'content' ); ?></div>
		<?php endif; ?>

		<!-- Liste de liens (optionel) -->
		<?php 
		$rows = get_field('links');
		if( $rows ) {
			foreach ($rows as $key => $row)  {
				$link = $row['link'];
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="link-red link-archive" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
				<?php 
			}
		}?>

	<?php endif; ?>

</section>
