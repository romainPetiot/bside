<?php
/**
 * Block Name: Text-column
 */
?>

<?php
$content = get_field('content');
$image = get_field('image');
$showImage = get_field('has-image');
$imagePosition = get_field('image_position');
?>
<section class="blk-text-column narrow-wrapper v-padding-small
    <?php
    if ( $showImage ) {
        echo "has-image ";
        
        if ( $imagePosition ) {
            echo "image-left";
        } 
        else {
            echo "image-right";
        }
    }
    else {
        echo "no-image ";
    }
    ?> 
    ">

    <?php if ( !$content ) :?>

    <div class="admin-infos">
        <b>Bloc Texte en colonnes</b><br>
        <p>Renseigner les informations</p>
    </div>

	<?php else : ?>

        <div class="text-column entry-content"><?php the_field( 'content' ); ?></div>

        <?php if( $image ): ?>
            
        <?php
        $size = 'bside-wrapper';
        $src = wp_get_attachment_image_src( $image, $size );
        $original = wp_get_attachment_image_src( $image, 'full' );
        $alt = get_post_meta( $id, '_wp_attachment_image_alt', true); 
        ?>

        <a href="<?php echo esc_url( $src[0] ); ?>" title="<?php echo esc_attr( $alt );?>">
            <img src="<?php echo esc_attr( $original[0] ); ?>" alt="<?php echo esc_attr( $alt );?>" />
        </a>

        <?php endif; ?>

    <?php endif; ?>

</section>