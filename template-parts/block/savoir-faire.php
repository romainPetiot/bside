
<section class="blk-savoir-faire">
<?php

if ( have_rows( 'savoir_faire_list' ) ) :

	while ( have_rows( 'savoir_faire_list' ) ) :
		the_row();

		?>
		
		<!-- Lien vers savoir-faire -->
		<a class="card-savoir-faire" href="<?php the_sub_field( 'savoir-faire-link' ); ?>" style="background-color:<?php the_sub_field( 'color' ); ?>">
			<!-- Picto -->
			<img src="<?php the_sub_field( 'picto' ); ?>" aria-hidden="true">
			<!-- Titre -->
			<h2 class="lead-title"><?php the_sub_field( 'title' ); ?></h2>
		</a>

		<?php

  endwhile;

endif;

?>