<?php $block_uniq_id = uniqid(); ?>

<section id="block_<?php echo $block_uniq_id; ?>_section" class="blk-video wrapper blocked-wrapper v-padding-regular">

  <!-- Video Thumbnail -->
  <?php 
  $videoThumbnail = get_field('picture');
  
  if ( $videoThumbnail ) {
    $image = $videoThumbnail ;
  } 
  else {
    $image = get_field('image-fallback', 'options'); // Fallback image
  } 
  $size = "bside-wrapper";
  ?>

  <div id="player_<?php echo $block_uniq_id; ?>" class="is-icon" role="button"></div>
  <?php 
    if( $image ) {
      echo wp_get_attachment_image( $image, $size, false, array( "id" => "block_".$block_uniq_id."_img") );
    }
  ?>
    
</section>

<script>

  // JS à définir en fontion des interractions souhaitées

  // Detect click on thumbnail
  document.querySelector('#block_<?php echo $block_uniq_id; ?>_img').addEventListener("click", function(e) {
    showIframe<?php echo $block_uniq_id; ?>();
  });
  // Detect click player icon
  document.querySelector('#player_<?php echo $block_uniq_id; ?>').addEventListener("click", function(e) {
    showIframe<?php echo $block_uniq_id; ?>();
    document.querySelector('#player_<?php echo $block_uniq_id; ?>').classList.remove("is-icon");
  });

  // ShowIframe
  function showIframe<?php echo $block_uniq_id; ?>() {
    document.querySelector('#block_<?php echo $block_uniq_id; ?>_section').innerHTML = '<div id="block_<?php echo $block_uniq_id; ?>_iframe"><?php the_field( "video_link" ); ?></div>';
  }
</script>
