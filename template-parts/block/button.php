<?php
/**
 * Block Name: Button
 */
?>
<section class="blk-button narrow-wrapper center v-padding-small">

	<?php
    $link = get_field('link');
    $link_url = $link['url'];
    $link_title = $link['title'];
    $link_target = $link['target'] ? $link['target'] : '_self';
	if ( !$link ) :
	?>

		<div class="admin-infos">
			<b>Bloc Bouton</b><br>
			<p>Renseigner les informations</p>
		</div>

	<?php else : ?>

        <!-- Text [optionel] -->
        <?php if ( get_field( 'content' ) ) : ?>
            <div class="center entry-content"><?php the_field( 'content' ); ?></div>
        <?php endif; ?>

        <!-- Button -->
        <a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo $link_title ?></a>
    
    <?php endif; ?>
    
</section>