<!-- Intro -->
<section id="hp-hero" class="wrapper title-like">
  <h1 id="hp-title"><?php echo get_field( 'hp-title' ); ?></h1>
  <p><?php echo get_field( 'hp-chapo' ); ?></p>

  <?php 
  // Video
  // Test if video exists
  if ( get_field( 'video_link' ) ) {
    ?>
    <a href="#" class="btn-modale"  data-field="https://www.youtube.com/watch?v=daBxtXqtc_8" data-uniq-id="<?php echo uniqid();?>">
      <div class="reset-style show-icon" aria-label="<?php _e( 'Lancer la vidéo', 'bside' ); ?>" ></div>
    </a> 

    <?php
  }
  ?>

  <?php
    // Thumbnail
    if ( has_post_thumbnail() ){
      ?>

      <div id="frontpage-hero" >
      <?php echo the_post_thumbnail( 'full-size' ); ?>
      </div>

      <?php
    }
  ?>

</section>

<div id="raw-content">
  <?php the_content(); ?>
</div>
