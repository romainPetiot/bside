<form>
  <!--
  <input type="email" placeholder="Votre email" required>
  <input type="submit" value="OK">
  <div class="checkbox">
    <input type="checkbox" id="rgpd" name="rgpd">
    <label for="rgpd">
      <?php // _e("J’accepte le traitement de mes données personnelles. En savoir plus sur ", "bside");?>
      <a class="link-default" href="<?php // echo get_privacy_policy_url() ?>">
        <?php // _e("notre politique de confidentialité.", "bside");?>
      </a>
    </label>
  </div>
  -->
</form>
<?php
if(get_field("mailchimp_api_key", 'option')):?>

<form id="footer-newslettter-form">
    <input type="hidden" name="honeyPotWhitePaper" value="">
    <input type="email" class="input-brd" name="newletter_email" id="newletter_email" placeholder="Entrez votre e-mail" required><br>
    <input class="footer-newsletter button no-margin" type="submit" id="sendMessageNewsletter" value="S'inscrire"><br>
    <p id="ResponseMessageNewsletter"></p>
    <!--<i class="formInfo">
        <?php _e("Les informations recueillies à partir de ce formulaire sont traitées par B Side pour vous inscrire à sa newsletter.<br/> Pour connaître vos droits et la politique de B Side sur la protection des données,", "bside");?>
        <a href="<?php echo get_privacy_policy_url() ?>">
            <?php _e("Cliquez ici", "bside");?>
        </a>
    </i>-->
</form>

<?php endif;?>