<?php $widget_id = 'widget_' . $widget_infos_args['widget_id']; ?>

<!-- Adresse -->
<p><?php echo nl2br(get_field( 'address', $widget_id )); ?></p>

<!-- Téléphone -->
<a href="tel:<?php the_field( 'telephone', $widget_id ); ?>"><?php the_field( 'telephone', $widget_id ); ?></a>

<!-- Lien contact -->
<a href="<?php the_field( 'contact_link', $widget_id ); ?>">Nous contacter</a>