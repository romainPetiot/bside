<?php $widget_id = 'widget_' . $widget_infos_args['widget_id']; ?>

<!-- Logo -->
<img style="width:100px;" src="<?php the_field( 'logo', $widget_id ); ?>" alt="">
<!-- Texte -->
<p><?php the_field( 'text', $widget_id ); ?></p>
