<!-- Lien -->
<a href="<?php the_permalink(); ?>">
  <!-- Image mise en avant -->
  <div style="background-image:url(<?php the_post_thumbnail_url(); ?>);">
	  <!-- Titre -->
	  <h2><?php the_title(); ?></h2>
	  <!-- Type -->
	  <p><?php the_field( 'realisation_type' ); ?></p>
  </div>
</a>