<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Susty
 */

?>

<article id="post-<?php the_ID(); ?>" class="single-real">

	<!-- Intro de l'article -->
	<header id="header-bg-desktop" class="center">
		<span id="header-bg-mobile">
			<!-- Titre -->
			<h1><?php the_title(); ?></h1>
			<img aria-hidden="true" src="<?php echo get_stylesheet_directory_uri(); ?>/image/flach-bas.png"  height="50" width="50">
		<span>
	</header>

	<?php  
	$id = get_post_thumbnail_id();
	$sizeMobile = 'bside-realisation-excerpt';
	$sizeDesktop = 'bside-post-thumbnail';
	$bgMobile = wp_get_attachment_image_src( $id, $sizeMobile );
	$bgDesktop = wp_get_attachment_image_src( $id, $sizeDesktop );
	?>

	<style>
		#header-bg-desktop {
			background-image: url( <?php echo esc_attr( $bgDesktop[0] ); ?> );
		}
		#header-bg-mobile {
			background-image: url( <?php echo esc_attr( $bgMobile[0] ); ?>);
		}
	</style>

	<!-- Contenu-->
	<div id="raw-content">
		<?php the_content(); ?>
	</div>

	<!-- Autres Réalisations -->
	<section id="realisation-menu">
		<div id="realisation-control" class="wrapper">
			<h2 class="center"><?php _e( 'Nos autres réalisations', 'bside' ); ?></h2>
		</div>
		<?php

		$realisation_objects = get_field( 'others_realisations' );

		if ( $realisation_objects ) :
			?>
				<div id="listing-realisation-caroussel">
				<?php foreach ( $realisation_objects as $post ) : ?>
						<?php
						setup_postdata( $post );

						//get_template_part( 'template-parts/single-realisation/others-realisations' );
						get_template_part( 'template-parts/archive-realisation' );

					endforeach;
				?>
				</div>
				<?php wp_reset_postdata(); ?>
			<?php

		else :

			$args = array(
				'post_type'      => 'realisation',
				'posts_per_page' => 4,
				'orderby'        => 'rand',
			);

			$wpquery_realisation = new WP_Query( $args );
			?>
			<div id="listing-realisation-caroussel">
			<?php

			if ( $wpquery_realisation->have_posts() ) :
				while ( $wpquery_realisation->have_posts() ) :
					$wpquery_realisation->the_post();

					//get_template_part( 'template-parts/single-realisation/others-realisations' );
					get_template_part( 'template-parts/archive-realisation' );

				endwhile;
			endif;
			?>
			</div>
			<?php

		endif;
		?>
	</section>
		
</article><!-- #post-<?php the_ID(); ?> -->
