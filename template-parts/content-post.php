<?php

/**
 * Template part for displaying post content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Susty
 */

?>

<article id="post-<?php the_ID(); ?>" class="single-post">

		<!-- Intro de l'article -->
		<!-- <header class="wrapper center v-padding-small"> -->
		<header id="header-bg-desktop" class="center">
			
			<span id="header-bg-mobile">
				<!-- Catégorie -->
				<?php the_category(); ?>
				<!-- Titre -->
				<h1><?php the_title(); ?></h1>
				<!-- Auteur + Date -->
				<p>By <?php the_author(); ?> | <time datetime="<?php echo get_the_date( 'c' ); ?>"><?php echo get_the_date(); ?></time> </p>
				<img aria-hidden="true" src="<?php echo get_stylesheet_directory_uri(); ?>/image/flach-bas.png" height="50" width="50">
			</span>
		</header>

		<!-- Image mise en avant -->
		<div class="wrapper center">

			<?php 
			
			// 1 - Find image

			if ( has_post_thumbnail() ) {
				// Post-thumbnail
				$id = get_post_thumbnail_id();
			} else {
				// Fallback image
				$id = get_field('image-fallback', 'options');
			} 

			// 2 - Display image
			 
			// $size = 'bside-post-thumbnail';
			$sizeMobile = 'bside-realisation-excerpt';
			$sizeDesktop = 'bside-post-thumbnail';
			$bgMobile = wp_get_attachment_image_src( $id, $sizeMobile );
			$bgDesktop = wp_get_attachment_image_src( $id, $sizeDesktop );
			// $src = wp_get_attachment_image_src( $id, $size );
			// $srcset = wp_get_attachment_image_srcset( $id, $size );
			// $sizes = wp_get_attachment_image_sizes( $id, $size );
			// $alt = get_post_meta( $id, '_wp_attachment_image_alt', true); 

			?>

			<!-- <img src="<?php //echo esc_attr( $src[0] );?>"
				srcset="<?php //echo esc_attr( $srcset ); ?>"
				sizes="<?php //echo esc_attr( $sizes );?>"
				alt="<?php //echo esc_attr( $alt );?>" /> -->

			<style>
				#header-bg-desktop {
					background-image: url( <?php echo esc_attr( $bgDesktop[0] ); ?> );
				}
				#header-bg-mobile {
					background-image: url( <?php echo esc_attr( $bgMobile[0] ); ?>);
				}
			</style>

		</div>

		<!-- Contenu de l'article -->
		<div id="raw-content">
			<?php the_content();?>
		</div>

		<!-- Partage -->
		<div class="wrapper center v-padding-regular">
			<a id="share-linkedin" class="JSrslink reset-style button-icon-label" href="https://www.linkedin.com/cws/share?url=<?php echo nbTinyURL(get_the_permalink());?>">
				<label><?php _e( 'Partager', 'bside' ); ?></label>
				<img alt="<?php _e( 'Linkedin', 'bside' ); ?>" src="<?php echo get_stylesheet_directory_uri(); ?>/image/linkedin-color.svg" height="16" width="16">
			</a>
			<a id="share-twitter" class="JSrslink reset-style button-icon-label" href="https://www.twitter.com/share?url=<?php echo nbTinyURL(get_the_permalink());?>">
				<label><?php _e( 'Tweeter', 'bside' ); ?></label>
				<img alt="<?php _e( 'Twitter', 'bside' ); ?>"  src="<?php echo get_stylesheet_directory_uri(); ?>/image/twitter-color.svg" height="16" width="20">
			</a>
		</div>

		<!-- Autres Posts -->
		<div id="post-suggestion" class="v-padding-regular wrapper">
			<h2 class="lead-paragraph blocked-wrapper margin-b"><?php _e( 'D\'autres articles', 'bside' ); ?></h2>
			<div id="listing-post-3col">

				<?php

				global $post;

				$lastposts = get_posts(
					array(
						'posts_per_page' => 3,
						'post_status'    => 'publish',
					)
				);

				if ( $lastposts ) :
					foreach ( $lastposts as $post ) :
						
						// Listing template : archive-post.php
						get_template_part( 'template-parts/archive-post' );

					endforeach;
					wp_reset_postdata();
				endif;
				?>

			</div>
		</div>
		
</article><!-- #post-<?php the_ID(); ?> -->
