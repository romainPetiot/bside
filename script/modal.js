document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("btn-modale");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            document.body.innerHTML += '<div class="modale" id="modale' + el.dataset.uniqId + '"><button class="closeModale" onclick="closeModale(\'modale' + el.dataset.uniqId + '\')">X</button><div class="modaleContent" id="modaleContent' + el.dataset.uniqId + '"></div></div>';

            var formData = new FormData();
            formData.append("field", el.dataset.field);
            xhr = new XMLHttpRequest(); //prepare la requete
            xhr.open('POST', resturl + 'ihag_modale', true);
            xhr.onload = function() {
                if (xhr.status === 200) {
                    document.getElementById('modaleContent' + el.dataset.uniqId).innerHTML = xhr.response;
                    document.getElementById('modale' + el.dataset.uniqId).classList.add("active");
                }
            };
            xhr.send(formData);
        });
    });

});


function closeModale(id) {
    var modal = document.getElementById(id);
    var garbage = document.body.removeChild(modal);
}