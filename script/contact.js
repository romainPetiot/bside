if (document.forms.namedItem("bsideForm")) {
    document.forms.namedItem("bsideForm").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("bsideForm");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', resturl + 'bsideForm', true);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessage').disabled = false;
                document.getElementById('ResponseAnchor').classList.add("messageSent");
                document.getElementById('ResponseMessage').classList.add("showResponseMessage");
            }
        };
        xhr.send(formData);
    });
}


if (document.forms.namedItem("footer-newslettter-form")) {
    document.forms.namedItem("footer-newslettter-form").addEventListener('submit', function(e) {
        document.getElementById('sendMessageNewsletter').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("footer-newslettter-form");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', resturl + 'formNewsletter', true);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessageNewsletter').disabled = false;
                document.getElementById('sendMessageNewsletter').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessageNewsletter').innerText = xhr.response.replace('"', '').replace('"', '');
                document.getElementById('ResponseMessageNewsletter').classList.add("showResponseMessage");
            }
        };
        xhr.send(formData);
    });
}