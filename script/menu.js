/*

Animate header.php (see header.scss)

File structure :
------------------
1 - Open & Close menu
------------------
2 - Detect click on #overlay
------------------
3 - Scroll detection

*/

// 1 - Open & Close menu (for mobile devices)
function toggleMenu() {
    document.getElementById("menu").classList.toggle("menu-open");
}

// 2 - Detect click on #overlay (for mobile menu)
// When we click outside of the mobile menu, the menu disappears
// We use #overlay to detect the click
document.addEventListener("DOMContentLoaded", function(event) {
    document.getElementById("overlay").addEventListener("click", function(e) {
        toggleMenu ();
    });
});

// 3 - Scroll detection
//topbar gets sticky on scroll
var scroll = document.getElementById("menu");
var menu = document.getElementById("menu");
var sticky = 0;
var isWhite = menu.dataset.white; 

window.onscroll = function() {

    if (window.pageYOffset > sticky) {
        menu.classList.add("menu-small");
        if (isWhite == true) {
            menu.classList.remove("menu-white");
        }
    } else {
        menu.classList.remove("menu-small");
        if (isWhite == true) {
            menu.classList.add("menu-white");
        }
    }
};