<?php
/**
 * Header logo white
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Susty
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-16px.png" sizes="16x16">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-96px.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-32px.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-124px.png" sizes="124x124">
	<link rel="apple-touch-icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-124px.png" />
	<meta name="theme-color" content="#0651f9">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>	

	<a class="skip-link" tabindex="0"  href="#content"><?php esc_html_e( 'Accéder au contenu', 'ihag' ); ?></a>
	<a class="skip-link" tabindex="0" href="#headerLinks"><?php esc_html_e( 'Menu', 'ihag' ); ?></a>
	<!--<a class="skip-link" tabindex="0"  href="#footer"><?php // esc_html_e( 'Accéder au pied de page', 'ihag' ); ?></a>-->

	<header id="menu" data-white="0" tabindex="0">

		<!-- Logo -->
		<a id="home_link" href="<?php echo get_home_url(); ?>" title="<?php echo get_bloginfo( 'name' ); echo get_bloginfo( 'description' ); ?>">
			<?php 
				$image = get_field('logo', 'option');
				//$size = '80-80'; 
				if( $image ) {
					echo wp_get_attachment_image( $image, $size );
				} 
			?>
		</a>

		<!-- Burger Menu -->
		<button id="burger-menu" class="reset-style desktop-hidden huge-hidden" onclick="toggleMenu()" aria-label="<?php esc_html_e( 'Ouvrir le menu', 'ihag' ); ?>">
			<img aria-hidden="true" src="<?php echo get_stylesheet_directory_uri(); ?>/image/burger.svg" height="18" width="24">
		</button>

		<div id="headerMenu">

			<!-- Close Menu -->
			<button id="close-menu" class="reset-style desktop-hidden huge-hidden" onclick="toggleMenu()" aria-label="<?php esc_html_e( 'Fermer le menu', 'ihag' ); ?>">
				<img aria-hidden="true" src="<?php echo get_stylesheet_directory_uri(); ?>/image/cross.svg" height="20" width="20">
			</button>

			<!-- Menu principal -->
			<nav id="headerLinks">
				<!-- Links -->
				<?php wp_nav_menu([
					'theme_location' => 'primary'
				]); ?>
			</nav>

			<!-- Social -->
			<div id="headerSocial">
				<?php
				$twitter = get_field('twitter_link', 'option');
				$linkedin = get_field('linkedin_link', 'option');
				$behance = get_field('behance_link', 'option');

				if( $twitter ) {
				?>
					<a target="_blank" href="<?php echo $twitter ?>" aria-label="<?php esc_html_e( 'Twitter', 'ihag' ); ?>">
						<img aria-hidden='true' src="<?php echo get_stylesheet_directory_uri(); ?>/image/twitter.svg" height="16" width="20">
					</a>
				<?php 
				} if( $linkedin ) {
				?>
					<a target="_blank" href="<?php echo $linkedin ?>" aria-label="<?php esc_html_e( 'LinkedIn', 'ihag' ); ?>">
						<img aria-hidden='true' src="<?php echo get_stylesheet_directory_uri(); ?>/image/linkedin.svg" height="16" width="16">
					</a>
				<?php 
				} if( $behance ) {
				?>
					<a target="_blank" href="<?php echo $behance ?>" aria-label="<?php esc_html_e( 'Behance', 'ihag' ); ?>">
						<img aria-hidden='true' src="<?php echo get_stylesheet_directory_uri(); ?>/image/behance.svg" height="16" width="25">
					</a>
				<?php 
				} ?>			
			</div>
		</div>

		<!-- Background trick for mobile --> 
		<div id="overlay" aria-hidden="true" class="desktop-hidden huge-hidden"></div>

	</header>

	<div id="content" tabindex="0">
