if (document.forms.namedItem("bsideForm")) {
    document.forms.namedItem("bsideForm").addEventListener('submit', function(e) {
        document.getElementById('sendMessage').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("bsideForm");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', resturl + 'bsideForm', true);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessage').disabled = false;
                document.getElementById('ResponseAnchor').classList.add("messageSent");
                document.getElementById('ResponseMessage').classList.add("showResponseMessage");
            }
        };
        xhr.send(formData);
    });
}


if (document.forms.namedItem("footer-newslettter-form")) {
    document.forms.namedItem("footer-newslettter-form").addEventListener('submit', function(e) {
        document.getElementById('sendMessageNewsletter').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("footer-newslettter-form");
        var formData = new FormData(form);
        xhr = new XMLHttpRequest();
        xhr.open('POST', resturl + 'formNewsletter', true);
        xhr.onload = function() {
            if (xhr.status === 200) {
                document.getElementById('sendMessageNewsletter').disabled = false;
                document.getElementById('sendMessageNewsletter').classList.add("hiddenSubmitButton");
                document.getElementById('ResponseMessageNewsletter').innerText = xhr.response.replace('"', '').replace('"', '');
                document.getElementById('ResponseMessageNewsletter').classList.add("showResponseMessage");
            }
        };
        xhr.send(formData);
    });
}
// organise - infinit scroll 
if (document.querySelector('#listing-post') || document.querySelector('#listing-post-3col')) {
    var base_url = window.location.href;
    if (document.querySelector('#listing-post')) {
        var elm = document.querySelector('#listing-post');
    } else {
        var elm = document.querySelector('#listing-post-3col');
    }

    // loader = document.querySelector('#loaderPost');
    var page = elm.dataset.page;
    var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    load = false;
    ticking = false;
    window.addEventListener('scroll', function(e) {
        if (!ticking) {
            window.setTimeout(function() {
                ticking = false;
                //Do something
                if (!load && (elm.offsetTop + elm.clientHeight) < (window.scrollY + height)) { //scroll > bas de infinite-list
                    //inserer les éléments suivants
                    if (page >= elm.dataset.nbPageMax) {
                        console.log(load);
                    } else {
                        load = true;
                        readMorePost();
                    }

                }
            }, 300); //fréquence du scroll
        }
        ticking = true;
    });
}



function readMorePost() {
    page++;
    // loader.classList.add("active");
    var formData = new FormData();
    formData.append("page", page);
    formData.append("cpt", elm.dataset.cpt);
    formData.append("taxo", elm.dataset.taxo);

    xhr = new XMLHttpRequest();
    xhr.open('POST', resturl + 'scroll', true);
    xhr.onload = function() {
        if (xhr.status === 200) {
            load = false;
            elm.insertAdjacentHTML('beforeend', xhr.response);
            //window.history.replaceState("", "", elm.dataset.url + "page/" + page + "/");
            // loader.classList.remove("active");
        }
    };
    xhr.send(formData);
}
/*

Animate header.php (see header.scss)

File structure :
------------------
1 - Open & Close menu
------------------
2 - Detect click on #overlay
------------------
3 - Scroll detection

*/

// 1 - Open & Close menu (for mobile devices)
function toggleMenu() {
    document.getElementById("menu").classList.toggle("menu-open");
}

// 2 - Detect click on #overlay (for mobile menu)
// When we click outside of the mobile menu, the menu disappears
// We use #overlay to detect the click
document.addEventListener("DOMContentLoaded", function(event) {
    document.getElementById("overlay").addEventListener("click", function(e) {
        toggleMenu ();
    });
});

// 3 - Scroll detection
//topbar gets sticky on scroll
var scroll = document.getElementById("menu");
var menu = document.getElementById("menu");
var sticky = 0;
var isWhite = menu.dataset.white; 

window.onscroll = function() {

    if (window.pageYOffset > sticky) {
        menu.classList.add("menu-small");
        if (isWhite == true) {
            menu.classList.remove("menu-white");
        }
    } else {
        menu.classList.remove("menu-small");
        if (isWhite == true) {
            menu.classList.add("menu-white");
        }
    }
};
document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("btn-modale");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            document.body.innerHTML += '<div class="modale" id="modale' + el.dataset.uniqId + '"><button class="closeModale" onclick="closeModale(\'modale' + el.dataset.uniqId + '\')">X</button><div class="modaleContent" id="modaleContent' + el.dataset.uniqId + '"></div></div>';

            var formData = new FormData();
            formData.append("field", el.dataset.field);
            xhr = new XMLHttpRequest(); //prepare la requete
            xhr.open('POST', resturl + 'ihag_modale', true);
            xhr.onload = function() {
                if (xhr.status === 200) {
                    document.getElementById('modaleContent' + el.dataset.uniqId).innerHTML = xhr.response;
                    document.getElementById('modale' + el.dataset.uniqId).classList.add("active");
                }
            };
            xhr.send(formData);
        });
    });

});


function closeModale(id) {
    var modal = document.getElementById(id);
    var garbage = document.body.removeChild(modal);
}
/*

Create Twitter and Linkedin link to share posts

*/

document.addEventListener('DOMContentLoaded', function () {
    var els = document.getElementsByClassName("JSrslink");
    var heightScreen = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    var widthScreen = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) { 
            e.stopPropagation();
            e.preventDefault();
            var width  = 320, 
            height = 400,
            left   = (widthScreen - width)  / 2, 
            top    = (heightScreen - height) / 2,
            url    = this.href,
            opts   = 'status=1' +
                             ',width='  + width  +
                             ',height=' + height +
                             ',top='    + top    +
                             ',left='   + left;
            window.open(url, 'myWindow', opts);
            return false;
        });
    });
});

/*

Animate frontpage.php (see frontpage.scss)

*/

// Video Home
function toggleVideoHome() {
    document.getElementById("videoContainer").classList.toggle("isActive");
}