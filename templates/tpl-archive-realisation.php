<?php
/**
 * Template Name: Archive Best-of
 */
get_header();
?>

<main class="after-topbar">

<?php
if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		?>
		
		<div id="raw-content">

			<?php the_content(); ?>
		
		</div>
		
		<?php
	endwhile;
endif;
?>

<section class="wrapper center v-padding-regular">

	<div id="listing-realisation">

	<?php
	$args = array(
		'post_type'      => 'realisation',
		'posts_per_page' => -1,
	);

	$wpquery_realisation = new WP_Query( $args );

	if ( $wpquery_realisation->have_posts() ) :
		while ( $wpquery_realisation->have_posts() ) :
			$wpquery_realisation->the_post();

			// Listing template : archive-realisation.php
			get_template_part( 'template-parts/archive-realisation' );

		endwhile;
	endif;
	?>

	</div>


</section>

</main>

<?php
get_footer();
