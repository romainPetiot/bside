<?php
/**
 * Template Name: Archive Equipe
 */
get_header();
?>

<main class="after-topbar">

<?php
if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		?>
			
			<div id="raw-content">
				<?php the_content(); ?>
			</div>
			
		<?php
	endwhile;
endif;
?>

	<section id="listing-staff">

	<?php

	$args = array(
		'post_type'      => 'staff',
		'posts_per_page' => -1,
	);

	$wpquery_realisation = new WP_Query( $args );

	if ( $wpquery_realisation->have_posts() ) :
		while ( $wpquery_realisation->have_posts() ) :
			$wpquery_realisation->the_post();
			?>
				<article  class="staff-card">

					<?php  
					// 1 - Find image
					$background = get_field( 'photo_1' );

					if ( $background ) {
						$id = $background; // Custom Portrait
					} else {
						$id = get_field('image-fallback', 'options'); // Fallback image
					} 

					$size = 'bside-staff';
					$srce = wp_get_attachment_image_src( $id, $size );
					?>

					<!-- Open button -->
					<button class="staff-button reset-style" onclick="removeStaff.call(this)" title="<?php the_title(); ?>" style="background-image:url( <?php echo esc_attr( $srce[0] ); ?>);">
						<h2><?php the_field( 'name' ); ?></h2>
						<p class="small-text"><?php the_field( 'fonction' ); ?></p>
					</button>

					<!-- Modal -->
					<div class="staff-modal">
						
						<!-- Staff : infos -->
						<div class="staff-infos">
							<h2><?php the_field( 'name' ); ?></h2>
							<p><?php the_field( 'fonction' ); ?></p>
							<div class="entry-content small-text"><?php the_field( 'presentation' ); ?></div>
							<!-- Social Links -->
							<?php
							$twitter = get_field('twitter');
							$linkedin = get_field('linkedin');

							if( $twitter ) {
							?>
								<a target="_blank" href="<?php echo $twitter ?>" aria-label="<?php esc_html_e( 'Twitter', 'ihag' ); ?>">
									<img aria-hidden='true' src="<?php echo get_stylesheet_directory_uri(); ?>/image/twitter.svg" height="16" width="20">
								</a>
							<?php 
							} if( $linkedin ) {
							?>
								<a target="_blank" href="<?php echo $linkedin ?>" aria-label="<?php esc_html_e( 'LinkedIn', 'ihag' ); ?>">
									<img aria-hidden='true' src="<?php echo get_stylesheet_directory_uri(); ?>/image/linkedin.svg" height="16" width="16">
								</a>
							<?php 
							}
							?>
						</div>

						<!-- Staff : picture -->
						<?php
						$image = get_field( 'photo_2' );
						
						// 1 - Find image

						if ( $image ) {
							$id = $image; // Custom Portrait
						} else {
							$id = get_field('image-fallback', 'options'); // Fallback image
						} 

						// 2 - Display image

						$size = 'bside-staff-zoom';
						$src = wp_get_attachment_image_src( $id, $size );
						$srcset = wp_get_attachment_image_srcset( $id, $size );
						$sizes = wp_get_attachment_image_sizes( $id, $size );
						$alt = get_post_meta( $id, '_wp_attachment_image_alt', true); 
						?>

						<img src="<?php echo esc_attr( $src[0] );?>"
							srcset="<?php echo esc_attr( $srcset ); ?>"
							sizes="<?php echo esc_attr( $sizes );?>"
							alt="<?php echo esc_attr( $alt );?>" />

					</div><!-- End of Modal -->

					<!-- Close button -->
					<button class="staff-close button-icon-label reset-style white" onclick="removeStaff.call(this)" title="<?php _e( 'Fermer la fenêtre', 'bside' ); ?>">
						<label><?php _e( 'Fermer', 'bside' ); ?></label>
						<img aria-hidden="true" src="<?php echo get_stylesheet_directory_uri(); ?>/image/cross.svg" height="20" width="20">
					</button>

					<!-- Overlay (Close) -->
					<button class="staff-overlay reset-style" onclick="removeStaff.call(this)" ></button>
					
				</article>
			<?php
		endwhile;
	endif;
	?>

	</section>

<script>

	function removeStaff(event) {
	this.parentElement.classList.toggle("isActive");
	}

</script>

</main>

<?php
get_footer();
