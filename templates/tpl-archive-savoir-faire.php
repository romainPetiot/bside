<?php
/**
 * Template Name: Archive Savoir-faire
 */
get_header();
?>

<main class="after-topbar">

<?php
if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		?>

		<div id="raw-content">
		
			<?php the_content(); ?>

		</div>

		<?php
	endwhile;
endif;
?>

</main>

<?php
get_footer();
