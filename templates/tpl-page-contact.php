<?php
/**
 * Template Name: Page contact
 */
get_header();
?>

<main class="after-topbar">

<?php
if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		?>

	<div id="page-<?php the_ID(); ?>">

		<div id="raw-content">

			<?php the_content();?>
		
		</div>

			<form name="bsideForm" id="bsideForm" action="#" method="POST" class="wrapper v-padding-regular">
				<input type="hidden" name="honeyPotbside" value="">
				<div>
					<label for="namebside">Votre prénom / Votre nom*</label>
					<input type="text" name="namebside" id="namebside" placeholder="<?php _e( 'John Doe', 'bside' ); ?>" required>
				</div>
				
				<div>
					<label for="emailbside">Votre adresse email*</label>
					<input type="email" name="emailbside" id="emailbside" placeholder="<?php _e( 'adresse@mail.com', 'bside' ); ?>" required>
				</div>
				
				<div>
					<label for="phonebside">Votre numéro de téléphone</label>
					<input type="tel" name="phonebside" id="phonebside" placeholder="<?php _e( '+33 6 11 22 33 44', 'bside' ); ?>" >
				</div>

				<div>
					<label for="messagebside">Votre message *</label>
					<textarea type="text" name="messagebside" id="messagebside" placeholder="<?php _e( 'Rédigez votre message …', 'bside' ); ?>" required></textarea>
				</div>

				<div class="checkbox">
					<input type="checkbox" id="rgpd" name="rgpd" required>
					<label for="rgpd">
						<?php _e("J’accepte le traitement de mes données personnelles. En savoir plus sur ", "bside");?>
						<a class="link-default" href="<?php echo get_privacy_policy_url() ?>" target="_blank">
							<?php _e("notre politique de confidentialité.", "bside");?>
						</a>
					</label>
				</div>

				<div id="ResponseAnchor">
					<input class="cta-standard" type="submit" id="sendMessage" value="Envoyer">
					<p id="ResponseMessage"><?php _e( 'Votre message a été envoyé !', 'bside' ); ?></p>
				</div>	
			</form>
		
	</div><!-- #page-<?php the_ID(); ?> -->

		<?php
	endwhile;
endif;
?>

</main>

<?php get_footer(); ?>
