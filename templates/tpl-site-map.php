<?php

/*
Template Name: Plan du site
*/

get_header();
?>

<main class="after-topbar">

	<?php
	while ( have_posts() ) :
		the_post();
		?>

			<div id="raw-content">

				<?php the_content();?>

			</div>

			<div id="site-map" class="narrow-wrapper">
				<?php 
				wp_nav_menu(
					array(
						'theme_location' => 'site-map',
					)
				);
				?>
			</div>

		<?php
	endwhile; // End of the loop.
	?>

</main><!-- #main -->

<?php get_footer(); ?>