

</div><!-- div #content -->

<footer id="footer" tabindex="0">
<h2 class="skip-link" >Informations complémentaires</h2>

	<div class="wrapper">

	<div id="footer-layout">

		<?php
			dynamic_sidebar('footer-1'); // Widget 'Infos'
			dynamic_sidebar('footer-2'); // Widget 'Contact + Newsletter'
			dynamic_sidebar('footer-3'); // Widget 'Actus'
			dynamic_sidebar('footer-4'); 
		?>

		<div id="footerCopyright">
			<!-- Label -->
			<?php 
			$image = get_field( 'label', 'option' );
			$size = 'full';
				if( $image ) {
					echo wp_get_attachment_image( $image, $size, false, array( "class" => "label" ) );
				}
			?>

			<!-- Links -->
			<span>
				<a href="<?php echo get_home_url(); ?>" title="<?php echo get_bloginfo( 'name' );?>" >© <?php esc_html_e( 'B Side', 'ihag' ); ?></a>
				<?php echo get_the_privacy_policy_link() ?>
				<?php $link = get_field('sitemap', 'option');
				if( $link ) {
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<a class="reset" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
				<?php
				} ?>
				<a href="<?php the_field("page-ml", 'option'); ?>" title="<?php _e("mention legales", "bside");?>" ><?php esc_html_e( 'Mentions légales', 'ihag' ); ?></a>
			</span>
		</div>

		<!-- Social -->
		<div id="footerSocial">

			<?php
			$twitter = get_field('twitter_link', 'option');
			$linkedin = get_field('linkedin_link', 'option');
			$behance = get_field('behance_link', 'option');

			if( $twitter ) {
			?>
				<a target="_blank" href="<?php echo $twitter ?>" aria-label="<?php esc_html_e( 'Twitter', 'ihag' ); ?>" >
					<img aria-hidden='true' src="<?php echo get_stylesheet_directory_uri(); ?>/image/twitter.svg" height="16" width="20">
				</a>
			<?php 
			} if( $linkedin ) {
			?>
				<a target="_blank" href="<?php echo $linkedin ?>" aria-label="<?php esc_html_e( 'LinkedIn', 'ihag' ); ?>">
					<img aria-hidden='true' src="<?php echo get_stylesheet_directory_uri(); ?>/image/linkedin.svg" height="16" width="16">
				</a>
			<?php 
			} if( $behance ) {
			?>
				<a target="_blank" href="<?php echo $behance ?>" aria-label="<?php esc_html_e( 'Behance', 'ihag' ); ?>">
					<img aria-hidden='true' src="<?php echo get_stylesheet_directory_uri(); ?>/image/behance.svg" height="16" width="25">
				</a>
			<?php 
			} ?>			
		</div>
		<!-- End of #footerSocial -->

	</div><!-- End of #footer-layout -->
		
	</div><!-- .wrapper -->
</footer>

<?php wp_footer(); ?>

</body>
</html>