<?php
get_header();
?>
	<main id="main" class="after-topbar">

		<div class="narrow-wrapper">

			<h1 class="margin-b"><?php the_field('blog-title', 'option') ?></h1>
			<p class="lead-paragraph"><?php the_field('blog-introduction', 'option') ?></p>

		</div>

		<?php $num_page = (get_query_var("paged") ? get_query_var("paged") : 1);?>
		
			<?php
			if ( have_posts() ) :
			?>
				<section class="listing-archive wrapper v-padding-regular"
			data-cpt="post"
			data-page="<?php echo $num_page;?>"
			data-nb-page-max="<?php echo ceil(($wp_query->found_posts)/(get_option('posts_per_page' ))); ?>"
			data-url="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>"
			data-taxo="0"
			id="listing-post-3col">

			<?php
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/archive', get_post_type() );
				endwhile;

			//the_posts_navigation();

			endif;
			?>

		</section>

	</main>
<?php
get_footer();
