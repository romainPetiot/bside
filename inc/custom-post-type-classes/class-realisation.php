<?php

class Bside_Cpt_Realisation {

	private const NAME = 'realisation';
	private const SLUG = 'best-of';

	public function __construct() {
		add_action( 'init', array( $this, 'register_realisation' ) );
	}

	public function register_realisation() {
		$labels  = array(
			'name'               => _x( 'Réalisations', 'Post Type General Name', 'bside' ),
			'singular_name'      => _x( 'Réalisation', 'Post Type Singular Name', 'bside' ),
			'menu_name'          => __( 'Réalisations', 'bside' ),
			'name_admin_bar'     => __( 'Réalisations', 'bside' ),
			'all_items'          => __( 'Toutes les réalisations', 'bside' ),
			'add_new_item'       => __( 'Ajouter une nouvelle réalisation', 'bside' ),
			'add_new'            => __( 'Ajouter un réalisation', 'bside' ),
			'new_item'           => __( 'Nouvelle réalisation', 'bside' ),
			'edit_item'          => __( 'Éditer la réalisation', 'bside' ),
			'update_item'        => __( 'Mettre à jour la réalisation', 'bside' ),
			'view_item'          => __( 'Voir la réalisation', 'bside' ),
			'view_items'         => __( 'Voir les réalisations', 'bside' ),
			'search_items'       => __( 'Chercher une réalisation', 'bside' ),
			'not_found'          => __( 'Réalisation non trouvée', 'bside' ),
			'not_found_in_trash' => __( 'Réalisation non trouvée dans la corbeille', 'bside' ),
		);
		$rewrite = array(
			'slug'       => self::SLUG,
			'with_front' => true,
			'pages'      => true,
			'feeds'      => true,
		);
		$args    = array(
			'label'               => __( 'Réalisations', 'bside' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'editor', 'thumbnail' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 20,
			'menu_icon'           => 'dashicons-portfolio',
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => false, // On utilise le template "Archive Best-of"
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'rewrite'             => $rewrite,
			'capability_type'     => 'page',
			'show_in_rest'        => true,
		);
		register_post_type( self::NAME, $args );
	}

}
