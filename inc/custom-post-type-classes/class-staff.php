<?php

class Bside_Cpt_Staff {

	private const NAME = 'staff';
	private const SLUG = 'equipe';

	public function __construct() {
		add_action( 'init', array( $this, 'register_staff' ) );
	}

	public function register_staff() {
		$labels  = array(
			'name'                  => _x( 'Équipe', 'Post Type General Name', 'bside' ),
			'singular_name'         => _x( 'Équipe', 'Post Type Singular Name', 'bside' ),
			'menu_name'             => __( 'Équipe', 'bside' ),
			'name_admin_bar'        => __( 'Équipe', 'bside' ),
			'all_items'             => __( 'Toute l\'équipe', 'bside' ),
			'add_new_item'          => __( 'Ajouter un nouveau membre', 'bside' ),
			'add_new'               => __( 'Ajouter un membre', 'bside' ),
			'new_item'              => __( 'Nouveau membre', 'bside' ),
			'edit_item'             => __( 'Éditer le membre', 'bside' ),
			'update_item'           => __( 'Mettre à jour le membre', 'bside' ),
			'view_item'             => __( 'Voir le membre', 'bside' ),
			'view_items'            => __( 'Voir les membres', 'bside' ),
			'search_items'          => __( 'Chercher un membre', 'bside' ),
			'not_found'             => __( 'Membre non trouvé', 'bside' ),
			'not_found_in_trash'    => __( 'Membre non trouvé dans la corbeille', 'bside' ),
		);
		$rewrite = array(
			'slug'       => self::SLUG,
			'with_front' => true,
			'pages'      => true,
			'feeds'      => true,
		);
		$args    = array(
			'label'               => __( 'Équipe', 'bside' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'editor' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 20,
			'menu_icon'           => 'dashicons-groups',
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'rewrite'             => $rewrite,
			'capability_type'     => 'page',
			'show_in_rest'        => true
		);
		register_post_type( self::NAME, $args );
	}

}
