<?php

class Bside_Cpt_Savoir_Faire {

	private const NAME = 'savoir-faire';
	private const SLUG = 'savoir-faire';

	public function __construct() {
		add_action( 'init', array( $this, 'register_savoir_faire' ) );
	}

	public function register_savoir_faire() {
		$labels  = array(
			'name'               => _x( 'Savoir-faire', 'Post Type General Name', 'bside' ),
			'singular_name'      => _x( 'Savoir-faire', 'Post Type Singular Name', 'bside' ),
			'menu_name'          => __( 'Savoir-faire', 'bside' ),
			'name_admin_bar'     => __( 'Savoir-faire', 'bside' ),
			'all_items'          => __( 'Tous les savoir-faire', 'bside' ),
			'add_new_item'       => __( 'Ajouter un nouveau savoir-faire', 'bside' ),
			'add_new'            => __( 'Ajouter un savoir-faire', 'bside' ),
			'new_item'           => __( 'Nouveau savoir-faire', 'bside' ),
			'edit_item'          => __( 'Éditer le savoir-faire', 'bside' ),
			'update_item'        => __( 'Mettre à jour le savoir-faire', 'bside' ),
			'view_item'          => __( 'Voir le savoir-faire', 'bside' ),
			'view_items'         => __( 'Voir les savoir-faire', 'bside' ),
			'search_items'       => __( 'Chercher un savoir-faire', 'bside' ),
			'not_found'          => __( 'Savoir-faire non trouvé', 'bside' ),
			'not_found_in_trash' => __( 'Savoir-faire non trouvé dans la corbeille', 'bside' ),
		);
		$rewrite = array(
			'slug'       => self::SLUG,
			'with_front' => true,
			'pages'      => true,
			'feeds'      => true,
		);
		$args    = array(
			'label'               => __( 'Savoir-faire', 'bside' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'editor', 'thumbnail'),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 20,
			'menu_icon'           => 'dashicons-art',
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => false, // on utilise le template de page "Archive Savoir-faire"
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'rewrite'             => $rewrite,
			'capability_type'     => 'page',
			'show_in_rest'        => true,
		);
		register_post_type( self::NAME, $args );
	}

}
