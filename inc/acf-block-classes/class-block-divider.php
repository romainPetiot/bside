<?php
class Bside_Acf_Block_Divider {

	public function __construct() {
		add_action( 'acf/init', array( $this, 'acf_block_divider' ) );
	}

	public function acf_block_divider() {
		if ( function_exists( 'acf_register_block_type' ) ) {

			acf_register_block_type(
				array(
					'name'            => 'divider',
					'title'           => __( 'Divider' ),
					'description'     => __( 'Divider : espace vertical entre 2 blocs' ),
					'placeholder'     => __( 'Divider' ),
					'render_template' => 'template-parts/block/divider.php',
					'category'        => 'bside',
					'mode'            => 'preview',
					'icon'            => 'minus',
					'keywords'        => array( 'divider', 'espace', 'marge' ),
					'supports'        => array(
						'align' => false,
						// 'multiple' => false,
						'mode'  => true,
					),
				)
			);

		}
	}

}
