<?php
class Bside_Acf_Block_Savoir_Faire {

	public function __construct() {
		add_action( 'acf/init', array( $this, 'acf_block_savoir_faire' ) );
	}

	public function acf_block_savoir_faire() {
		if ( function_exists( 'acf_register_block_type' ) ) {

			acf_register_block_type(
				array(
					'name'            => 'savoir-faire',
					'title'           => __( 'Savoir-faire' ),
					'description'     => __( 'Savoir-faire' ),
					'placeholder'     => __( 'Savoir-faire' ),
					'render_template' => 'template-parts/block/savoir-faire.php',
					'category'        => 'bside',
					'mode'            => 'preview',
					'icon'            => 'awards',
					'keywords'        => array( 'savoir-faire' ),
					'supports'        => array(
						'align' => false,
						// 'multiple' => false,
						'mode'  => true,
					),
				)
			);

		}
	}

}
