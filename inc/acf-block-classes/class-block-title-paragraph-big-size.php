<?php
class Bside_Acf_Block_Title_Paragraph_Big_Size {

	public function __construct() {
		add_action( 'acf/init', array( $this, 'acf_block_title_paragraph_big_size' ) );
	}

	public function acf_block_title_paragraph_big_size() {
		if ( function_exists( 'acf_register_block_type' ) ) {

			acf_register_block_type(
				array(
					'name'            => 'title-paragraph-big-size',
					'title'           => __( 'Titre + Paragraphe' ),
					'description'     => __( 'Titre + Paragraphe' ),
					'placeholder'     => __( 'Titre + Paragraphe' ),
					'render_template' => 'template-parts/block/title-paragraph-big-size.php',
					'category'        => 'bside',
					'mode'            => 'preview',
					'icon'            => 'text',
					'keywords'        => array( 'texte', 'paragraphe'),
					'supports'        => array(
						'align' => false,
						// 'multiple' => false,
						'mode'  => true,
					),
				)
			);

		}
	}

}
