<?php
class Bside_Acf_Block_Video {

	public function __construct() {
		add_action( 'acf/init', array( $this, 'acf_block_video' ) );
	}

	public function acf_block_video() {
		if ( function_exists( 'acf_register_block_type' ) ) {

			acf_register_block_type(
				array(
					'name'            => 'bside-video',
					'title'           => __( 'B Side Vidéo' ),
					'description'     => __( 'B Side Vidéo' ),
					'placeholder'     => __( 'B Side Vidéo' ),
					'render_template' => 'template-parts/block/video.php',
					'category'        => 'bside',
					'mode'            => 'preview',
					'icon'            => 'format-video',
					'keywords'        => array( 'vidéo',),
					'supports'        => array(
						'align' => false,
						// 'multiple' => false,
						'mode'  => true,
					),
				)
			);

		}
	}

}
