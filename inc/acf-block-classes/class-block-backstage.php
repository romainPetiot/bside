<?php
class Bside_Acf_Block_Backstage {

	public function __construct() {
		add_action( 'acf/init', array( $this, 'acf_block_backstage' ) );
	}

	public function acf_block_backstage() {
		if ( function_exists( 'acf_register_block_type' ) ) {

			acf_register_block_type(
				array(
					'name'            => 'backstage',
					'title'           => __( 'Backstage' ),
					'description'     => __( 'Backstage' ),
					'placeholder'     => __( 'Backstage' ),
					'render_template' => 'template-parts/block/backstage.php',
					'category'        => 'bside',
					'mode'            => 'preview',
					'icon'            => 'clipboard',
					'keywords'        => array( 'backstage' ),
					'supports'        => array(
						'align' => false,
						// 'multiple' => false,
						'mode'  => true,
					),
				)
			);

		}
	}

}
