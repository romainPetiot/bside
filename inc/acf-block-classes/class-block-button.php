<?php
class Bside_Acf_Block_Button {

	public function __construct() {
		add_action( 'acf/init', array( $this, 'acf_block_button' ) );
	}

	public function acf_block_button() {
		if ( function_exists( 'acf_register_block_type' ) ) {

			acf_register_block_type(
				array(
					'name'            => 'button',
					'title'           => __( 'Button' ),
					'description'     => __( 'Button' ),
					'placeholder'     => __( 'Button' ),
					'render_template' => 'template-parts/block/button.php',
					'category'        => 'bside',
					'mode'            => 'preview',
					'icon'            => 'admin-links',
					'keywords'        => array( 'bouton', 'button', 'lien' ),
					'supports'        => array(
						'align' => false,
						// 'multiple' => false,
						'mode'  => true,
					),
				)
			);

		}
	}

}
