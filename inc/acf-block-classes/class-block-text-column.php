<?php
class Bside_Acf_Block_Text_Column {

	public function __construct() {
		add_action( 'acf/init', array( $this, 'acf_block_text_column' ) );
	}

	public function acf_block_text_column() {
		if ( function_exists( 'acf_register_block_type' ) ) {

			acf_register_block_type(
				array(
					'name'            => 'text-column',
					'title'           => __( 'Texte en colonne' ),
					'description'     => __( 'Texte en colonne' ),
					'placeholder'     => __( 'Texte en colonne' ),
					'render_template' => 'template-parts/block/text-column.php',
					'category'        => 'bside',
					'mode'            => 'preview',
					'icon'            => 'editor-paragraph',
					'keywords'        => array( 'text', 'colonne', 'texte' ),
					'supports'        => array(
						'align' => false,
						// 'multiple' => false,
						'mode'  => true,
					),
				)
			);

		}
	}

}
