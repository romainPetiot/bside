<?php
class Bside_Acf_Block_Last_Post {

	public function __construct() {
		add_action( 'acf/init', array( $this, 'acf_block_last_post' ) );
	}

	public function acf_block_last_post() {
		if ( function_exists( 'acf_register_block_type' ) ) {

			acf_register_block_type(
				array(
					'name'            => 'last-post',
					'title'           => __( 'Derniers articles' ),
					'description'     => __( 'Derniers articles' ),
					'placeholder'     => __( 'Derniers articles' ),
					'render_template' => 'template-parts/block/last-post.php',
					'category'        => 'bside',
					'mode'            => 'preview',
					'icon'            => 'format-image',
					'keywords'        => array( 'actualité', 'derniers', 'articles', 'post' ),
					'supports'        => array(
						'align' => false,
						//'multiple' => false,
						'mode'  => true,
					),
				)
			);

		}
	}

}
