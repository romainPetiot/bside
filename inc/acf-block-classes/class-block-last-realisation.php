<?php
class Bside_Acf_Block_Last_Realisation {

	public function __construct() {
		add_action( 'acf/init', array( $this, 'acf_block_last_realisation' ) );
	}

	public function acf_block_last_realisation() {
		if ( function_exists( 'acf_register_block_type' ) ) {

			acf_register_block_type(
				array(
					'name'            => 'last-realisation',
					'title'           => __( 'Dernières réalisations' ),
					'description'     => __( 'Dernières réalisations' ),
					'placeholder'     => __( 'Dernières réalisations' ),
					'render_template' => 'template-parts/block/last-realisation.php',
					'category'        => 'bside',
					'mode'            => 'preview',
					'icon'            => 'media-document',
					'keywords'        => array( 'réalisations', 'dernières', 'realisations' ),
					'supports'        => array(
						'align' => false,
						//'multiple' => false,
						'mode'  => true,
					),
				)
			);

		}
	}

}
