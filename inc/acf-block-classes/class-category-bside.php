<?php

class Bside_Block_Category {

	public function __construct() {
		add_filter( 'block_categories', array( $this, 'block_category' ), 10, 2 );
	}

	public function block_category( $categories, $post ) {
		return array_merge(
			$categories,
			array(
				array(
					'slug'  => 'bside',
					'title' => __( 'B Side', 'bside' ),
					'icon'  => 'star-empty',
				),
			)
		);
	}

}
