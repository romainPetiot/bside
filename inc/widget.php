<?php

bside_load_classes( 'widget-classes' );

function bside_widgets() {
	register_widget( 'Bside_Widget_Contact' );
	register_widget( 'Bside_Widget_Newsletter' );
	register_widget( 'Bside_Widget_Infos' );
}

add_action( 'widgets_init', 'bside_widgets' );

function bside_sidebars_register() {

	register_sidebar(
		array(
			'name' => 'Footer emplacement 1 : Infos',
			'id'   => 'footer-1',
			"before_title" => "<h3>",
    		"after_title" => "</h3>"
		)
	);

	register_sidebar(
		array(
			'name' => 'Footer emplacement 2 : Contact',
			'id'   => 'footer-2',
			"before_title" => "<h3>",
    		"after_title" => "</h3>"
		)
	);

	register_sidebar(
		array(
			'name' => 'Footer emplacement 3 : Newsletter',
			'id'   => 'footer-3',
			"before_title" => "<h3>",
    		"after_title" => "</h3>"
		)
	);

	register_sidebar(
		array(
			'name' => 'Footer emplacement 4 : Actus',
			'id'   => 'footer-4',
		)
	);

}

add_action( 'widgets_init', 'bside_sidebars_register' );
