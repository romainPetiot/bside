<?php

bside_load_classes( 'acf-block-classes' );

// Catégorie de blocks "B Side"
new Bside_Block_Category();

// Block Last Post
new Bside_Acf_Block_Last_Post();

// Block "Titre + Paragraphe Big Size"
new Bside_Acf_Block_Title_Paragraph_Big_Size();

// Block Last Realisation
new Bside_Acf_Block_Last_Realisation();

// Block Vidéo (avec image)
new Bside_Acf_Block_Video();

// Block Savoir-faire
new Bside_Acf_Block_Savoir_Faire();

// Block Backstage
new Bside_Acf_Block_Backstage();

// Block Divider
new Bside_Acf_Block_Divider();

// Block Button
new Bside_Acf_Block_Button();

// Block Text columns
new Bside_Acf_Block_Text_Column();

// Ajoute une feuille de styles  dans l'admin
// add_theme_support('editor-styles');
// add_editor_style(get_stylesheet_directory_uri().'/assets/css/admin-editor.css');
function my_theme_setup() {

	// Nouveauté à ajouter
	add_theme_support( 'editor-styles' );

	// Puis la même fonction qu'on utilisait auparavant pour Tiny MCE
	add_editor_style( 'style-editor.css' );
	// add_editor_style(get_stylesheet_directory_uri().'/assets/css/admin-editor.css');
	// add_theme_support( 'align-wide' );
}
add_action( 'after_setup_theme', 'my_theme_setup' );

// FILTRES LES BLOCS AUTORISES SUR LE SITE
// cf. https://rudrastyh.com/gutenberg/remove-default-blocks.html
// -----------------------------------------------------------------------------
add_filter( 'allowed_block_types', 'nb_allowed_block_types', 10, 2 );
function nb_allowed_block_types( $allowed_blocks, $post ) {
	$allowed_blocks = array(

		// Blocs spécifiques du theme
		'acf/title-paragraph-big-size',
		'acf/last-post',
		'acf/last-realisation',
		'acf/bside-video',
		'acf/savoir-faire',
		'acf/backstage',
		'acf/divider',
		'acf/button',
		'acf/text-column',
		// Blocs communs
		'core/heading',
		'core/paragraph',
		'core/image',
		// 'core/gallery',
		'core/list',
		'core/quote',
		// 'core/audio',
		// 'core/cover-image',
		// 'core/file',
		// 'core/video',
		// 'core/media',

	// Mise en forme
	// 'core/table',
	// 'core/verse',
	// 'core/code',
	// 'core/freeform',
	// 'core/html',
	// 'core/preformatted',
	// 'core/pullquote',

	// Mise en page
	'core/button',
	// 'core/columns',
	// 'core/media-text',
	// 'core/more',
	// 'core/nextpage',
	// 'core/separator',
	// 'core/spacer',

	// Widgets
	// 'core/shortcode',
	// 'core/archives',
	// 'core/categories',
	// 'core/latest-comments',
	// 'core/latest-posts',

	// Contenus embarqués
	// 'core/embed',
	// 'core-embed/youtube',
	// 'core-embed/facebook',
	// 'core-embed/twitter',
	// 'core-embed/instagram',
	// core-embed/wordpress
	// core-embed/soundcloud
	// core-embed/spotify
	// core-embed/flickr
	// core-embed/vimeo
	// core-embed/animoto
	// core-embed/cloudup
	// core-embed/collegehumor
	// core-embed/dailymotion
	// core-embed/funnyordie
	// core-embed/hulu
	// core-embed/imgur
	// core-embed/issuu
	// core-embed/kickstarter
	// core-embed/meetup-com
	// core-embed/mixcloud
	// core-embed/photobucket
	// core-embed/polldaddy
	// core-embed/reddit
	// core-embed/reverbnation
	// core-embed/screencast
	// core-embed/scribd
	// core-embed/slideshare
	// core-embed/smugmug
	// core-embed/speaker
	// core-embed/ted
	// core-embed/tumblr
	// core-embed/videopress
	// core-embed/wordpress-tv
	);

	if ( $post->ID == get_option( 'page_on_front' ) ) {
		$allowed_blocks[] = 'acf/carrousel-home';
	}

	return $allowed_blocks;
}




// DESACTIVE GUTENBERG DE CERTAINS TEMPLATES DE PAGE
// -----------------------------------------------------------------------------
add_filter( 'gutenberg_can_edit_post_type', 'ea_disable_gutenberg', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'ea_disable_gutenberg', 10, 2 );

// Désactive Gutenberg par template
function ea_disable_gutenberg( $can_edit, $post_type ) {

	if ( is_page_template( 'templates/tpl-range-recipe.php' ) ) {
		return true;
	}

	if ( $post_type == 'farmer' ) {
		return false;
	}
	if ( $post_type == 'recipe' ) {
		return false;
	}

	if ( ! ( is_admin() && ! empty( $_GET['post'] ) ) ) {
		return $can_edit;
	}

	return $can_edit;
}



// NETTOYAGE DES OPTIONS DE LA SIDEBAR
// cf. https://joseph-dickson.com/removing-specific-gutenberg-core-blocks-and-options/
// -----------------------------------------------------------------------------


// PALETTE DE COULEURS
// -----------------------------------------------------------------------------
// Désactive la palette de couleurs
add_theme_support( 'disable-custom-colors' );

// Supprime la palette de couleur
add_theme_support( 'editor-color-palette' );


// FONTS
// -----------------------------------------------------------------------------
// Désactive les tailles de typos
add_theme_support( 'disable-custom-font-sizes' );

// Crée une liste de choix de typos adaptée à la charte du site
add_theme_support(
	'editor-font-sizes',
	array(
		array(
			'name'      => 'Petit',
			'shortName' => 'S',
			'size'      => 12,
			'slug'      => 'small',
		),
		array(
			'name'      => 'Standard',
			'shortName' => 'D',
			'size'      => 16,
			'slug'      => 'standard',
		),
		array(
			'name'      => 'Moyen',
			'shortName' => 'M',
			'size'      => 20,
			'slug'      => 'medium',
		),
		array(
			'name'      => 'Grand',
			'shortName' => 'L',
			'size'      => 24,
			'slug'      => 'large',
		),
		array(
			'name'      => 'Très grand',
			'shortName' => 'XL',
			'size'      => 28,
			'slug'      => 'extra-large',
		),
	)
);
