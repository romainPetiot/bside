<?php

/*
 * Initialise les Custom Post-Types créés
 *
*/

bside_load_classes( 'custom-post-type-classes' );

// Savoir-faire
new Bside_Cpt_Savoir_Faire();

// Réalisations
new Bside_Cpt_Realisation();

// Equipe
new Bside_Cpt_Staff();
