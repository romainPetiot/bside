<?php

class Bside_Widget_Infos extends WP_Widget {

	function __construct() {
		parent::__construct(
			'bside_widget_infos',
			__( 'Infos', 'bside' ),
			array( 'description' => __( 'Affiche le logo et les infos', 'bside' ) )
		);
	}
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		set_query_var( 'widget_infos_args', $args );
		get_template_part('template-parts/widgets/infos');
	}
	public function form( $instance ) {
		if ( isset( $instance['title'] ) ) {
			$title = $instance['title'];
		} else {
			$title = __( '', 'bside' );
		}
	}
	public function update( $new_instance, $old_instance ) {
		$instance          = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}

}
