<?php
add_action( 'init', 'contact_custom_post_type' );
function contact_custom_post_type() {
	register_post_type(
		'ihag_contact', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		array(
			'labels'             => array(
				'name'          => __( 'Form Contact', 'ihag' ), /* This is the Title of the Group */
				'singular_name' => __( 'Form Contact', 'ihag' ), /* This is the individual type */
			), /* end of arrays */
			'menu_position'      => 18, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon'          => 'dashicons-email-alt', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical'       => false,
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => false,
			'query_var'          => false,
			'supports'           => array( 'title', 'editor' ),
		) /* end of options */
	);
}


/*
* traitement du post du form de Contact
* enregistrement des values dans le custom post type
*/
add_action(
	'rest_api_init',
	function() {
		register_rest_route(
			'bside',
			'bsideForm',
			array(
				'methods'             => 'POST', // WP_REST_Server::READABLE,
				'callback'            => 'ihag_contactForm',
				'permission_callback' => array(),
				'args'                => array(),
			)
		);

		register_rest_route( 'bside', 'formNewsletter', 
			array(
				'methods' 				=> 'POST', //WP_REST_Server::READABLE,
				'callback'        		=> 'ihagFormNewsletter',
				'permission_callback' 	=> array(),
				'args' 					=> array(),
			)
		);
	}
);

function ihag_contactForm( WP_REST_Request $request ) {
	if ( empty( $_POST['honeyPotbside'] ) ) {

		/*
		$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lce1FIUAAAAAC2kWX5qMMyHpfdW-IyRX927KGji&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
		$obj = json_decode($response);
		if($obj->success == false){//si pb captcha
			die();
		}*/

		global $wpdb;

		$upload_file_text = '';
		$attachments      = array();
		/*
		if(isset($_FILES['file'])){
			$maintenant = date("d-m-Y_H:i:s");
			$upload_dir   = wp_upload_dir();
			$uploaddirimg = $upload_dir['basedir'].'/img-form/';
			mkdir($uploaddirimg, 0755);
			$uploadfile = $uploaddirimg . $maintenant . '-'.basename($_FILES['file']['name']);
			if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
				array_push($attachments, $uploadfile);
				$upload_file_text = "Fichier : ".$upload_dir['baseurl'].'/img-form/' . $maintenant . '-'.basename($_FILES['file']['name']);
			}
		}*/

		$subject = __( 'Nouveau message depuis votre site web', 'ihag' );

		$body = 'Nom : ' . sanitize_text_field( $_POST['namebside'] ) . "\r\n";
		$body .= 'email : ' . sanitize_text_field( $_POST['emailbside'] ) . "\r\n";
		$body .= 'Téléphone : ' . sanitize_text_field( $_POST['phonebside'] ) . "\r\n";
		$body .= 'Message : ' . sanitize_textarea_field( $_POST['messagebside'] ) . "\r\n";
		

		$body     .= $upload_file_text;
		$headers[] = 'From: ' . get_bloginfo( 'name' ) . ' <' . __( 'no-reply@', 'ihag' ) . str_replace( 'www.', '', $_SERVER['SERVER_NAME'] ) . '>';
		wp_mail( get_option( 'admin_email' ), $subject, $body, $headers, $attachments );
		$post['post_type']    = 'ihag_contact';
		$post['post_status']  = 'publish';
		$post['post_title']   = sanitize_text_field( $_POST['namebside'] );
		$post['post_content'] = $body;
		$post_id              = wp_insert_post( $post, true );

		
		add_post_meta( $post_id, 'phone', sanitize_text_field($_POST['phonebside']), true );
		add_post_meta( $post_id, 'name', sanitize_text_field($_POST['namebside']), true );
		add_post_meta( $post_id, 'email', sanitize_text_field($_POST['emailbside']), true );
		add_post_meta( $post_id, 'comment', sanitize_textarea_field($_POST['messagebside']), true );

	}
	return new WP_REST_Response( '', 200 );
}


// wp_dashboard_setup is the action hook
add_action( 'wp_dashboard_setup', 'ihag_dashboard_contact' );
function ihag_dashboard_contact() {
	wp_add_dashboard_widget( 'ihag_dashboard_contact', 'Export CSV', 'ihag_dashboard_contact_export' );
}
function ihag_dashboard_contact_export() {
	?>
	<ul>
		<?php
		$args    = array(
			'posts_per_page' => 5,
			'post_type'      => 'ihag_contact',
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) :
			echo '<li><a href="' . admin_url( 'post.php?post=' . $post->ID . '&action=edit' ) . '">' . $post->post_title . '</a></li>';
		endforeach;
		?>
	</ul>
	<p>
	<a href="?report=ihag_export" class="button">Export</a>
	</p>
	<?php
}
class ihag_CSVExport {

	/**
	 * Constructor
	 */
	public function __construct() {
		if ( isset( $_GET['report'] ) && $_GET['report'] == 'ihag_export' ) {
			$this->ihag_export();
		}
	}
	public function ihag_export() {
		global $wpdb, $post;
		$csv_fields      = array();
		$csv_fields[]    = 'Name';
		$csv_fields[]    = 'Email';
		$csv_fields[]    = 'Phone';
		$csv_fields[]    = 'Comment';
		$output_filename = 'contact_' . date( 'Y-m-d H:i:s' ) . '.csv';
		$output_handle   = @fopen( 'php://output', 'w' );
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Content-Description: File Transfer' );
		header( 'Content-type: text/csv' );
		header( 'Content-Disposition: attachment; filename=' . $output_filename );
		header( 'Expires: 0' );
		header( 'Pragma: public' );
		// Insert header row
		fputcsv( $output_handle, $csv_fields, ';' );
		$args    = array(
			'posts_per_page' => -1,
			'post_type'      => 'ihag_contact',
		);
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) :
			setup_postdata( $post );
			$tab_data = array(
				get_post_meta( $post->ID, 'name', true ),
				get_post_meta( $post->ID, 'email', true ),
				get_post_meta( $post->ID, 'phone', true ),
				get_post_meta( $post->ID, 'comment', true ),
			);
			fputcsv( $output_handle, $tab_data, ';' );
		endforeach;
		wp_reset_postdata();
		fclose( $output_handle );
		exit();
	}
}
// Instantiate a singleton of this plugin
new ihag_CSVExport();



function ihagFormNewsletter(WP_REST_Request $request){
	
	if (empty($_POST['honeyPot'])) {
		if(addUserMailChimp (sanitize_email( $_POST['newletter_email'] ))){
			echo "Votre adresse a bien été enregistrée.";
		}
		else{
			echo "Votre adresse existe dans notre base de données.";
		}
	}		
	
	return new WP_REST_Response( '', 200 );
}

function addUserMailChimp($email){
	// API to mailchimp ########################################################
	$list_id = get_field("list_id", 'option');
	$authToken = get_field("mailchimp_api_key", 'option');
	// The data to send to the API

	$postData = array(
		"email_address" => $email, 
		"status" => "subscribed", 
		/*"merge_fields" => array(
		"FNAME"=> $_POST["name"],
		"PHONE"=> $_POST["phone"])*/
	);

	// Setup cURL
	$ch = curl_init('https://us11.api.mailchimp.com/3.0/lists/'.$list_id.'/members/');
	curl_setopt_array($ch, array(
		CURLOPT_POST => TRUE,
		CURLOPT_RETURNTRANSFER => TRUE,
		CURLOPT_HTTPHEADER => array(
			'Authorization: apikey '.$authToken,
			'Content-Type: application/json'
		),
		CURLOPT_POSTFIELDS => json_encode($postData)
	));
	// Send the request
	//	$response = curl_exec($ch);
	$result = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
	return ($httpCode == 200);
}
