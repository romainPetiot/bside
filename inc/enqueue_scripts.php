<?php
add_action('wp_enqueue_scripts', 'site_scripts', 999);
function site_scripts()
{
    global $wp_styles;
    wp_enqueue_style('styles', get_stylesheet_directory_uri() . '/style.css');

    if(!get_field('jquery_on_off', 'option')) {
        wp_deregister_script('jquery');
    }

    wp_register_script('script', get_stylesheet_directory_uri() . '/script.js', false, false, 'all');
    wp_enqueue_script('script');

    wp_localize_script('script', 'resturl', get_bloginfo('url') . '/wp-json/bside/');
}