<?php

// fonction de chargement des fichiers de classes
function bside_load_classes( $classes_dir ) {
	$bside_classes = glob( __DIR__ . '/inc/' . $classes_dir . '/*.php' );
	foreach ( $bside_classes as $class ) {
		require_once $class;
	}
}

require_once 'inc/rewrite-urls.php';
require_once 'inc/acf.php';
require_once 'inc/acf-block.php';
require_once 'inc/widget.php';
require_once 'inc/clean.php';
require_once 'inc/no-comment.php';
require_once 'inc/images.php';
require_once 'inc/custom-post-type.php';
require_once 'inc/breadcrumb.php';
require_once 'inc/menu.php';
require_once 'inc/enqueue_scripts.php';
require_once 'inc/contact.php';
require_once 'inc/modale.php';

// Supprime la taxo Tags des Articles
function bside_unregister_taxonomy() {
	register_taxonomy( 'post_tag', array() );
}
add_action( 'init', 'bside_unregister_taxonomy' );

// Adding excerpt for page
// add_post_type_support( 'page', 'excerpt' );

/**
 * Filter the excerpt length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
// add_filter( 'excerpt_length', function( $length ) { return 20; } );

// Copier le contenu d'une page ou d'un post à la création d'une traduction
function cw2b_content_copy( $content ) {
	if ( isset( $_GET['from_post'] ) ) {
		$my_post = get_post( $_GET['from_post'] );
		if ( $my_post ) {
			return $my_post->post_content;
		}
	}
	return $content;
}
// add_filter( 'default_content', 'cw2b_content_copy' );

function my_pre_get_posts( $query ) {

	if ( ! is_admin() && $query->is_main_query() && $query->is_search() ) {
		$query->set( 'post_type', array( 'post', 'rendez-vous', 'solution' ) );
		$query->set( 'posts_per_page', -1 );
	}

}
// add_action( 'pre_get_posts', 'my_pre_get_posts' );

function nbTinyURL( $url ) {
	$ch      = curl_init();
	$timeout = 5;
	curl_setopt( $ch, CURLOPT_URL, 'http://tinyurl.com/api-create.php?url=' . $url );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );
	$data = curl_exec( $ch );
	curl_close( $ch );
	return $data;
}

// fonction scroll
function ihag_scroll(WP_REST_Request $request){

	$offset = ( (int)sanitize_text_field( $_POST["page"] ) - 1) * get_option('posts_per_page' );
	$args = array(
	  'posts_per_page' => get_option('posts_per_page' ),
	  'post_type'   => $_POST['cpt'],
	  'post_status' => 'publish',
	  'offset'  => $offset,
	);
  
  
	if(isset($_POST["taxo"]) && !empty($_POST["taxo"]) && $_POST["taxo"] > 0 ){
	  $args['tax_query'] = array(
		array(
		  'taxonomy' => 'category',
		  'field'    => 'term_id',
		  'terms'    => (int) sanitize_text_field($_POST["taxo"])
		),
	  );
	}
	
	$custom_query = new WP_Query($args);
	if ( $custom_query->have_posts() ) : 
	  while ( $custom_query->have_posts() ) : 
		  $custom_query->the_post();
			  get_template_part( 'template-parts/archive', get_post_type() );
	  endwhile;
	endif;
  
	return new WP_REST_Response( NULL, 200 );
}


/*
* traitement du post du form de Contact
* enregistrement des values dans le custom post type
*/
add_action('rest_api_init', function() {
	register_rest_route( 'bside', 'scroll',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'ihag_scroll',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
});
