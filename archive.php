<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

get_header();
?>

	<main id="main" class="after-topbar">

		<div class="narrow-wrapper">

			<h1 class="margin-b"><?php the_archive_title() ?></h1>
			<?php the_archive_description('<p class="lead-paragraph">', '</p>') ?>

		</div>

		<?php $num_page = (get_query_var("paged") ? get_query_var("paged") : 1);?>
		<?php if ( have_posts() ) : ?>
			<section class="listing-archive wrapper v-padding-regular"
				data-cpt="post"
				data-page="<?php echo $num_page;?>"
				data-nb-page-max="<?php echo ceil(($wp_query->found_posts)/(get_option('posts_per_page' ))); ?>"
				data-url="<?php echo get_category_link(get_queried_object()->term_id);?>"
				data-taxo="0"
				id="listing-post">

			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/archive', get_post_type() );
			endwhile;

		the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</section>

	</main><!-- #main -->

<?php
get_footer();
